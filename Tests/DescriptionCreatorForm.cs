﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tests
{
    public partial class DescriptionCreatorForm : Form
    {
        public string Description { get; set; }
        public DescriptionCreatorForm(string description)
        {
            InitializeComponent();
            Description = description;
            webBrowser1.DocumentText = description;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            webBrowser1.DocumentText = textBox1.Text;
            Description = textBox1.Text;
        }
    }
}
