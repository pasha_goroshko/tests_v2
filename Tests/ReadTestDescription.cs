﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tests
{
    public partial class ReadTestDescription : Form
    {
        public ReadTestDescription(string textHtml)
        {
            InitializeComponent();
            if (!string.IsNullOrWhiteSpace(textHtml))
                webBrowser1.DocumentText = textHtml;
            else
            {
                webBrowser1.DocumentText = "<h1><b>Упс..</b></h1><p><b>Кажется автор забыл добавить описание теста.</b></p><p><img src=\"https://camo.githubusercontent.com/472c00f642bd004e55ba0771541138593eb23a53/687474703a2f2f6564756d6f74652e636f6d2f6173736574732f696d616765732f736c696465722f6e6f7464617461666f756e642e706e67\"></p>";
            }
        }
    }
}
